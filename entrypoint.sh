#!/bin/bash

set -eu


say() {
    >&2 echo "$( date '+%F %T') Docker: $*"
}

# We need to wait for the MariaDB root password in order
# to create database later on  
while [ ! -f /data/mariadb/mariadb_pwd ]
do
  sleep 2
done

MARIADB_PWD="$(cat /data/mariadb/mariadb_pwd)"

if [ -z "${MOODLE_URL+x}" ]; then
    MOODLE_URL="http://moodle.$(echo "$APPLICATION_ROOT" | cut -d ':' -f1)"
    export MOODLE_URL
    export DB_HOST="moodle.app.mariadb"
else
    export DB_HOST="127.0.0.1"
fi

mkdir -p /run/moodle/{sessions,cache,temp,localcache} /data/moodledata

readonly src_dir=/data/moodle
readonly old_src_dir=/data/moodle-prev-do-not-touch

say "Creating symlinks for temp files"
rm -rf /data/moodledata/temp && ln -s /run/moodle/temp /data/moodledata/temp
rm -rf /data/moodledata/cache && ln -s /run/moodle/cache /data/moodledata/cache
rm -rf /data/moodledata/localcache && ln -s /run/moodle/localcache /data/moodledata/localcache

if [[ ! -f /data/.initialized ]]; then
    say "Fresh installation, performing Moodle first time setup"

    say "Installing new moodle"
    rsync -az /app/code/ /data/moodle

    # Create moodle database and admin user
    mysql -uroot -p"${MARIADB_PWD}" -h "$DB_HOST" -e "CREATE DATABASE moodle DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"
    mysql -uroot -p"${MARIADB_PWD}" -h "$DB_HOST" -e "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO 'admin'@'%' IDENTIFIED BY 'admin';"
    
    say "Configure Moodle with a new user"
    php ${src_dir}/admin/cli/install_database.php --lang=en --adminuser=admin --adminpass=admin \
        --fullname='My Moodle Site' --shortname='MySite' --agree-license
    
    say "Download moodle database"
    wget https://s3.eu-central-1.wasabisys.com/olip-packages-prod/moodle/moodle-database-bsf.sql -O /tmp/moodle-database-bsf.sql
    mysql -uroot -p"${MARIADB_PWD}" -h "$DB_HOST" moodle < /tmp/moodle-database-bsf.sql

    touch /data/.initialized
    say "Fresh install: done."
elif ! grep -q "$RELEASE_BRANCH" /data/moodle/version.php; then
    say "Version has changed. Will upgrade"
    say "Moving existing installation"
    rm -rf "${old_src_dir}" && mv "${src_dir}" "${old_src_dir}"
    say "Installing new moodle"
    rsync -az /app/code/ /data/moodle

    # https://docs.moodle.org/dev/Plugin_types
    say "Copying over plugins from old installation"
    for subdir in $( php /app/scripts/plugintypes.php ); do
        [[ ! -d "${old_src_dir}/${subdir}" ]] && continue

        say "Copying ${subdir}"
        for x in $( find "${old_src_dir}/${subdir}"/* -maxdepth 0 -type d -printf "%f\n" ); do
            [[ -d "${src_dir}/${subdir}/${x}" ]] && continue  # already copied over from previous run?
            say "Copying ${x}"
            cp -rf "${old_src_dir}/${subdir}/${x}" "${src_dir}/${subdir}"
        done
    done

    say "Upgrading moodle"
    # https://docs.moodle.org/39/en/Administration_via_command_line#Upgrading
    php "${src_dir}/admin/cli/upgrade.php" --non-interactive

    say "Removing old moodle installation"
    rm -rf "${old_src_dir}"

    say "Upgrade: done."
fi

say "Ensure to pick correct upstream IP"
php "${src_dir}/admin/cli/cfg.php" --name=getremoteaddrconf --set="1"

say "Enforcing files ownership"
chown -R www-data:www-data /run/moodle /data/moodle /data/moodledata

# shellcheck disable=SC1091
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"

say "Lauching supervisord"
exec "$@"
