# hadolint ignore=DL3007
FROM offlineinternet/olip-base:latest

RUN mkdir -p /app/code /app/scripts
WORKDIR /app/code

# hadolint ignore=DL3008,DL3015
RUN apt-get update && \
    apt-get install software-properties-common -y && \
    add-apt-repository ppa:ondrej/php && \
    apt-get update && \
    apt-get install -y \
        apache2 \
        composer \
        php7.3 \
        crudini \
        rsync \
        mysql-client \
        php7.3-apcu \
        php7.3-bcmath \
        php7.3-bz2 \
        php7.3-cgi \
        php7.3-cli \
        php7.3-common \
        php7.3-curl \
        php7.3-dba \
        php7.3-dev \
        php7.3-enchant \
        php7.3-fpm \
        php7.3-gd \
        php7.3-geoip \
        php7.3-gmp \
        php7.3-gnupg \
        php7.3-imagick \
        php7.3-imap \
        php7.3-interbase \
        php7.3-intl \
        php7.3-json \
        php7.3-ldap \
        php7.3-mailparse \
        php7.3-mbstring \
        php7.3-mysql \
        php7.3-odbc \
        php7.3-opcache \
        php7.3-pgsql \
        php7.3-phpdbg \
        php7.3-pspell \
        php7.3-readline \
        php7.3-redis \
        php7.3-soap \
        php7.3-sqlite3 \
        php7.3-sybase \
        php7.3-tidy \
        php7.3-uuid \
        php7.3-xml \
        php7.3-xmlrpc \
        php7.3-xsl \
        php7.3-zip \
        php7.3-zmq \
        libapache2-mod-php7.3 \
        php-date \
        php-pear \
        php-twig \
        php-validate \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN a2disconf other-vhosts-access-log \
    && rm /etc/apache2/sites-enabled/* \
    && sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf \
    && echo "Listen 8000" > /etc/apache2/ports.conf \
    && a2enmod rewrite headers php7.3 \
    && crudini --set /etc/php/7.3/apache2/php.ini PHP upload_max_filesize 64M \
    && crudini --set /etc/php/7.3/apache2/php.ini PHP post_max_size 64M \
    && crudini --set /etc/php/7.3/apache2/php.ini PHP memory_limit 128M \
    && crudini --set /etc/php/7.3/apache2/php.ini Session session.save_path /run/moodle/sessions \
    && crudini --set /etc/php/7.3/apache2/php.ini Session session.gc_probability 1 \
    && crudini --set /etc/php/7.3/apache2/php.ini Session session.gc_divisor 100 \
    && cp /etc/php/7.3/apache2/php.ini /etc/php/7.3/cli/php.ini

COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf
COPY apache/moodle.conf /etc/apache2/sites-enabled/moodle.conf

ENV MOODLE_VERSION=3.11.0
ENV RELEASE_BRANCH=311
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN curl -L https://github.com/moodle/moodle/archive/v${MOODLE_VERSION}.tar.gz \
        | tar zx --strip-components 1 -C /app/code/

COPY config.php /app/code/config.php
COPY plugintypes.php entrypoint.sh /app/scripts/

COPY supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/supervisord.log /var/log/supervisor/supervisord.log

COPY scripts/* /usr/local/bin/

EXPOSE 8000

ENTRYPOINT ["/app/scripts/entrypoint.sh"]

CMD ["/usr/bin/supervisord"]
