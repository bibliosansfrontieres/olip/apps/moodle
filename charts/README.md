# Deploy a new instance 

```
cd charts
kubectl create namespace olip
helm upgrade  --install --namespace="olip" olip .
```

read `values.yaml` to override values from command line with `--set projectName="idc-bsf-prod"` for instance

You can as well define if you want to use persistente storage for your instance 

```
helm upgrade  --install --namespace="olip" --set persistentStorage.enabled="true" --set persistentStorage.olipStorageSize="100Gi" olip .
```

# Destroy
```
helm delete olip
```
